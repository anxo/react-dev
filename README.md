# React-Dev
Just another scaffold for your React projects.
Universal (isomorphic) rendering is already configured for you.
Powered by Webpack, Sass, React-Router and others.

## Usage
To start coding, just run:

  npm start

And you're all set.

To deploy, everything is configured for Dokku/Heroku support. Just git push to your server and everything should just work.
