// Generates a static build
import webpack from 'webpack'

// Config
import config from './webpack-config.js'

// Compile it
const compiler = webpack(config)
compiler.run((err, stats) => {
  if (err) throw new Error('webpack:build', err)
  console.log('[webpack:build]', stats.toString({
    chunks: false,
    colors: true
  }))
})
