// Global Webpack config
// Will be fine-tuned later for each individual build
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import CopyWebpackPlugin from 'copy-webpack-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'

export const config = {
  entry: './src/main.js',
  output: {
    path: 'dist',
    filename: 'main.js'
  },
  devtool: 'source-map',
  resolve: {
    root: path.resolve('./src'),
    extensions: ['', '.js', '.jsx', 'json']
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap')
      }, {
        test: /\.(scss|sass)$/,
        loader: ExtractTextPlugin.extract('style', [
          'css?sourceMap',
          'sass?sourceMap'
        ])
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.htm',
      template: 'src/index.htm',
      inject: 'body',
      minify: {
        collapseWhitespace: true
      }
    }),
    new CopyWebpackPlugin([{ from: 'src/static' }]),
    new ExtractTextPlugin('style.css')
  ]
}

export default config
