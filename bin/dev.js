// Starts the development server
import webpack from 'webpack'

// Config
import config from './webpack-config.js'

// Start watching
const compiler = webpack(config)
compiler.watch({
  aggregateTimeout: 300
}, (err, stats) => {
  if (err) throw new Error('webpack:build', err)
  console.log('[webpack:watch]', stats.toString({
    chunks: false,
    colors: true
  }))
})
